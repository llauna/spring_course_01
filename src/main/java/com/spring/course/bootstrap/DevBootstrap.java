package com.spring.course.bootstrap;

import com.spring.course.model.Author;
import com.spring.course.model.Book;
import com.spring.course.model.Publisher;
import com.spring.course.repository.AuthorRepository;
import com.spring.course.repository.BookRepository;
import com.spring.course.repository.PublisherRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private PublisherRepository publisherRepository;

    public DevBootstrap(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void onApplicationEvent (ContextRefreshedEvent contextRefreshedEvent){
        initData();
    }

    private void initData(){
        Publisher publisher = new Publisher();
        publisher.setName("foo");

        publisherRepository.save(publisher);

        Author eric = new Author("Eric", "Evans");
        Book ddd = new Book("Domain", "1234", publisher );
        eric.getBooks().add(ddd);
        ddd.getAuthors().add(eric);

        authorRepository.save(eric);
        bookRepository.save(ddd);

        Author david = new Author("Rod", "Johnson");
        Book a123 = new Book("Class of Spring", "123456", publisher);
        david.getBooks().add(a123);
        a123.getAuthors().add(david);

        authorRepository.save(david);
        bookRepository.save(a123);

    }


}
